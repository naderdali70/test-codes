import java.util.Arrays;

class odd {
    static void MissingNumb(int odd[], int sizeOdd){

        int minOdd = Integer.MAX_VALUE;
        int maxOdd = Integer.MIN_VALUE;

        int sumOddarr = 0;

        for (int i = 0 ; i <sizeOdd; i++) {
            minOdd= Math.min(minOdd, odd[i]);
            maxOdd= Math.max(maxOdd, odd[i]);
            sumOddarr += odd[i];

        }
        int totalTerms= 0 , reqSum=0;
        totalTerms = (minOdd / 2 ) + 1 ;
        int oddSumMin = totalTerms * totalTerms;
        totalTerms = (maxOdd / 2 ) +1 ;

        int oddSumMax = totalTerms * totalTerms;
        reqSum = oddSumMax - oddSumMin + minOdd;
        System.out.println("The missing odd number in the array given is : " + (reqSum - sumOddarr));


    }
    public static void main(String[] args) {
        int odd[]= { 1,3,7,9,11,13,15};
        int sizeOdd = odd.length ;
        System.out.println(Arrays.toString(odd));

        MissingNumb(odd,sizeOdd);


    }
}