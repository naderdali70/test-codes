import java.text.NumberFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final byte MONTHS_IN_YEAR = 12;
        final byte PERCENT = 100  ;
        int principal = 0;
        float monthlyInterest = 0 ;
        int numberOfPayments = 0 ;

        Scanner scanner = new Scanner(System.in);


         while(true) {
             System.out.println("Principal( $1K - $1M) : ");
              principal = scanner.nextInt();

             if (principal > 1000 && principal < 1000000)
                 break;
             System.out.println("Enter a number between 1,000$ and 1,000,000$ ! ");
         }
         while(true){
             System.out.println("Annual Interest Rate ");
             float  r = scanner.nextFloat();
             if (r > 0 && r < 30) {
                 monthlyInterest = r / MONTHS_IN_YEAR / PERCENT;
                 break;
             }
             System.out.println("Enter a number between 0 and 30 ! ");
         }


          while(true) {
              System.out.println("Period( years ) :");
              int  Period = scanner.nextInt();
              if (Period > 1 && Period < 30) {
                  numberOfPayments = Period * MONTHS_IN_YEAR;
                  break;
              }

              System.out.println("Enter a number between 1 and 30 !");
          }


            double mortgage = principal *
                       (monthlyInterest * Math.pow(1+monthlyInterest,numberOfPayments))
                               / (Math.pow(1+monthlyInterest,numberOfPayments)-1);
        String mortgageFormat = NumberFormat.getCurrencyInstance().format(mortgage);
        System.out.println("Mortgage : " +mortgageFormat);


    }
}